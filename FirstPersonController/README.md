# First Person Controller
This project was built in [Unity 2021.3.23f11](https://unity.com/releases/editor/archive/).
It features a First Person Character Controller with the following features:

 - Moving and Looking around
 - Sprinting
 - Jumping
 - Crouching
 - View Bobbing
 - Customizable Torch
 - Sliding on Slopes
 - Zooming in
 - Interaction System
 - Footstep Sounds
 - Health System
 - Stamina System

All of these are completly modular and can be turned on and off with just a switch inside of the Unity Editor.
## How to use

 1. Download a Source Code package from the [latest release.](https://gitlab.com/SerenoV7/firstpersoncontroller/-/releases)
 2. Unpack it inside a directory somewhere (as an example `C:\Users\your_username\Documents\UnityProjects\FirstPersonController\`)
 3. Go to the Unity Hub and click `Open > Add Project from Disk` and select the folder where you extracted the project files. 

 ![](https://cdn-sereno.pages.dev/images/static/unity-addproject.png)
 
 4. Start using the controller by dragging in the Player prefab.
