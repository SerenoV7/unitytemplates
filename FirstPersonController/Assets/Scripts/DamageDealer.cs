using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{

    [SerializeField] private float damageAmount = default;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) FirstPersonController.OnTakeDamage(damageAmount);
    }
}
