using UnityEngine;
using UnityEngine.AI;

public class Follower : MonoBehaviour
{
    [SerializeField] private Transform target;
    private NavMeshAgent agent;
    private SphereCollider trigger;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        trigger = GetComponent<SphereCollider>();
    }

    private void FixedUpdate() => agent.SetDestination(target.position);

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) 
        {
            FirstPersonController.OnTakeDamage(10);
            agent.SetDestination(transform.position);
        }
    }
    
}
